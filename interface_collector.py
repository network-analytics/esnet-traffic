#!/usr/bin/env python
#
# Based on https://github.com/ATLAS-Analytics/NetworkWeatherService/blob/master/ESNet/InterfaceCollector.py
# Author: Marian Babik
# Copyright CERN 2017
#
#

import sys
import time
import threading
import json
import requests
import argparse
from datetime import datetime

from messaging.message import Message
from messaging.queue.dqs import DQS

API_KEY = sys.argv[3]
URL = 'https://my.es.net/graphql_token'
AUTH = None


class Interface:
    def __init__(self, name, has_flow=True, tags=[]):
        self.name = name
        self.has_flow = has_flow
        self.tags = tags
        self.lastInterfaceUpdate = datetime.utcnow()
        self.lastFlowUpdate = datetime.utcnow()

    def __str__(self):
        return '"interface: %10s\t flow: %s\t tags: %s' % (self.name, self.has_flow, self.tags)


def get_interfaces():
    interfaces = []
    print("Getting interfaces...")
    entities_q = """ query { networkEntities(entityType:"LHCONE") { shortName hasFlow tags } }  """

    try:
        r = requests.get(URL, dict(query=entities_q), headers=dict(Authorization='Token ' + API_KEY))

        if r.status_code == 200:
            entities = r.json()
            # print(entities)
            for e in entities['data']['networkEntities']:
                interfaces.append(interface(e['shortName'], e['hasFlow'], e['tags']))
        else:
            print 'got status {0}: {1}'.format(r.status_code, r.content)
    except Exception:
        print ("Unexpected error in getting Interfaces:", sys.exc_info()[0])

    print ("Done.")
    for i in interfaces:
        i.prnt()
    return interfaces


def get_interface_data(interface):
    # print ("Loading interface data for: ",i.name)
    currenttime = datetime.utcnow()

    res = []

    interface_q = """
    query {
      networkEntity(shortName: "%s", entityType: "LHCONE") {
        interfaces (beginTime: "%s", endTime:"%s") { device interface traffic }
      }
    }""" % (interface.name, interface.lastInterfaceUpdate.isoformat(), currenttime.isoformat())

    try:
        r = requests.get(URL, dict(query=interface_q), headers=dict(Authorization='Token ' + API_KEY))

        if r.status_code != 200:
            print 'got status {0}: {1}'.format(r.status_code, r.content)
            return res

        dat = r.json()
        ins = dat['data']['networkEntity']['interfaces']
        # print(ins)

        d = datetime.utcnow()
        ind = "esnet_" + str(d.year) + "-" + str(d.month)
        data = {
            '_index': ind,
            '_type': 'interface',
            'site': interface.name
        }

        for s in ins:
            data['device'] = s["device"]
            data['interface'] = s["interface"]
            st = json.loads(s['traffic'])
            traf = st["points"]
            data['description'] = st["name"]
            for sample in traf:
                data['timestamp'] = sample[0]
                data['rateIn'] = long(sample[1])
                data['rateOut'] = long(sample[2])
                res.append(data.copy())
        print(interface.name, 'got', len(res), "interface results.")
        interface.lastInterfaceUpdate = currenttime
        return res

    except Exception:
        print ("Unexpected error:", sys.exc_info()[0])

    return res


def get_flow_data(interface):
    # print ("Loading flow data for: ",i.name)
    currenttime = datetime.utcnow()
    res = []

    try:

        flow_q = """
    	query {
      	networkEntity(shortName:"%s", entityType:"LHCONE") {
        flow(breakdown: "vpnsite" beginTime: "%s", endTime: "%s") { name traffic }
        }}""" % (interface.name, interface.lastFlowUpdate.isoformat(), currenttime.isoformat())

        r = requests.get(URL, dict(query=flow_q), headers=dict(Authorization='Token ' + API_KEY))

        if r.status_code != 200:
            print 'flow got status {0}: {1}'.format(r.status_code, r.content)
            return res

        dat = r.json()
        flows = dat['data']['networkEntity']['flow']
        # print(flows)

        d = datetime.now()
        ind = "esnet_" + str(d.year) + "-" + str(d.month)
        data = {
            '_index': ind,
            '_type': 'flow',
            'site1': interface.name
        }

        for f in flows:
            data['site2'] = f["name"].split("(")[0]
            st = json.loads(f['traffic'])
            traf = st["points"]
            for sample in traf:
                data['timestamp'] = sample[0]
                data['rateIn'] = sample[1]
                data['rateOut'] = sample[2]
                res.append(data.copy())
        print(interface.name, 'got', len(res), "flow results.")
        interface.lastFlowUpdate = currenttime
        return res

    except:
        print ("Unexpected error in flow data parsing: ", sys.exc_info()[0])
    return res


def loader(interface):
    print ("starting a thread for ", interface.name)
    while True:
        idata = get_interface_data(interface)
        try:
            mq = DQS(path='/var/spool/esnet')
            for entry in idata:
                mq_header = {'collector': "esnet-collector@cern",
                             'destination': "/topic/netflow.esnet"}
                mq_body = {'data': entry}
                msg = Message(body=json.dumps(mq_body), header=mq_header)
                msg.is_text = True
                mq.add_message(msg)
        except Exception as e:
            print ('Something seriously wrong during publishing. ', sys.exc_info())

        time.sleep(900)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ESNet flow collector')
    parser.add_argument('--version', action='version', version='%(prog)s 0.1')
    parser.add_argument('-a', '--api-key', dest='api_key', help='Specify API key for ESNet API')
    args = parser.parse_args()
    global API_KEY
    if not args.api_key:
        parser.print_help()
        sys.exit(-1)
    else:
        API_KEY = args.api_key

    interfaces = get_interfaces()
    # staggered start loaders threads
    for interface in interfaces:
        time.sleep(20)
        t = threading.Thread(target=loader, args=(interface,))
        t.daemon = True
        t.start()

    main_thread = threading.currentThread()
    for t in threading.enumerate():
        if t is main_thread:
            continue
        print 'joining %s', t.getName()
        t.join()
