#!/bin/bash
set -e

echo "================================================"
echo "esnet-traffic version: 0.0.01 Copyright CERN 2017"
echo "License: https://gitlab.cern.ch/network-analytics/esnet-traffic/LICENSE"
echo ""

echo "Starting messaging client ..."
/usr/bin/stompclt --conf /etc/stompclt/stompclt_esnet_output.cfg --pidfile /var/run/stompclt_esnet_output.pid --daemon

echo "Starting esnet-traffic collector ..."
mkdir -p /var/spool/esnet
cd /esnet-traffic
/usr/bin/python /esnet-traffic/interface_collector.py



