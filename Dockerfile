FROM centos:7

LABEL maintainer "Marian Babik <Marian.Babik@cern.ch>"
LABEL description "esnet-traffic image"
LABEL version "0.1"

RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install stompclt python-messaging python-dirq perl-IO-Socket-SSL git
RUN yum -y install python-requests
RUN git clone https://gitlab.cern.ch/network-analytics/esnet-traffic.git

COPY ./docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
